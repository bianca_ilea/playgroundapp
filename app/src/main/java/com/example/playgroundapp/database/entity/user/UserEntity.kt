package com.example.playgroundapp.database.entity.user

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "users")
data class UserEntity (

    @PrimaryKey(autoGenerate = true)
    var id: Int,

    var firstName: String,
    val lastName: String,
    var username: String,
    var password: String,
    var email: String)
//    var birthday: Date)