package com.example.playgroundapp.database.entity.event

enum class EventType {
    ACCIDENT,
    CATASTROPHE,
    RIOT,
    DANGER,
    TRAFFIC,
    WASTE
}