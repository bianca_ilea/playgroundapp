package com.example.playgroundapp.database.entity.event

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Event(
    @PrimaryKey var id: Int,
    var description: String,
    var image: Int,
    var score: Int) {
//    var additionalImages: List<Int>,
//    var location: EventLocation,
//    var type: EventType,
//    var reportTime: Date){

    fun confirm(){
        score++;
    }
}