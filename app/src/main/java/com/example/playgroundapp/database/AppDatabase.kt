package com.example.playgroundapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.playgroundapp.database.dao.EventDao
import com.example.playgroundapp.database.dao.UserDao
import com.example.playgroundapp.database.entity.event.Event
import com.example.playgroundapp.database.entity.user.UserEntity


@Database(entities = [UserEntity::class, Event::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun eventDao(): EventDao
}