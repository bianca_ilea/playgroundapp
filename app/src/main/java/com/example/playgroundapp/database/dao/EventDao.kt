package com.example.playgroundapp.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.playgroundapp.database.entity.event.Event
import com.example.playgroundapp.database.entity.user.UserEntity

@Dao
interface EventDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(event: Event)

}