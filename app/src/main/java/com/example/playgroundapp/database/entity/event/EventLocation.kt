package com.example.playgroundapp.database.entity.event

data class EventLocation(var coordX: Int, var coordY: Int)