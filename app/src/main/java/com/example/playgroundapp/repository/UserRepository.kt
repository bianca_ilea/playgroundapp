package com.example.playgroundapp.repository

import com.example.playgroundapp.database.AppDatabase
import com.example.playgroundapp.database.entity.user.UserEntity

class UserRepository(database: AppDatabase) {

    private val userDao = database.userDao()

    suspend fun insert(user: UserEntity) {
        userDao.insert(user)
    }

    suspend fun userAlreadyExists(username: String): Boolean {
        val user = userDao.getUserByUsername(username)
        return user != null
    }

    suspend fun areCredentialsValid(username: String, password: String): Boolean {
        val user = userDao.getUserByUsername(username)
        return user?.password.equals(password)
    }
}