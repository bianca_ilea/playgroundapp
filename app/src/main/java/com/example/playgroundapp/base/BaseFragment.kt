package com.example.playgroundapp.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.app.ActivityCompat.finishAfterTransition
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<VM : BaseViewModel, VB : ViewDataBinding>(@LayoutRes private val layoutRes: Int) : Fragment() {

    abstract val viewModel: VM
    lateinit var binding: VB

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    protected fun startActivityIntent(intent: Intent, activity: Activity) =
        startActivity(intent.apply {
            flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        }).also { closeActivity(activity) }

    private fun closeActivity(activity: Activity) = finishAfterTransition(activity)
}