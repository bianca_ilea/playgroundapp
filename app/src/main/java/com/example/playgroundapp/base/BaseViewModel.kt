package com.example.playgroundapp.base

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel()