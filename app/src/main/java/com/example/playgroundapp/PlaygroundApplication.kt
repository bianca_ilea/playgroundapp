package com.example.playgroundapp

import android.app.Application
import com.example.playgroundapp.di.repoModule
import com.example.playgroundapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PlaygroundApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@PlaygroundApplication)
            modules(listOf(viewModelModule, repoModule))
        }
    }
}