package com.example.playgroundapp.extensions

import android.view.View
import androidx.databinding.BindingAdapter

private var lastClickTime: Long = 0L
private const val MINIMAL_TIME_UNTIL_NEXT_BUTTON_CLICK = 600L

@BindingAdapter("onClickWithDebounce")
fun View.onClickWithDebounce(click: () -> Unit) {

    setOnClickListener {
        if (System.currentTimeMillis() - lastClickTime < MINIMAL_TIME_UNTIL_NEXT_BUTTON_CLICK) {
            return@setOnClickListener
        } else {
            click.invoke()
        }
        lastClickTime = System.currentTimeMillis()
    }

}