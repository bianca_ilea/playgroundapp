package com.example.playgroundapp.extensions

import androidx.databinding.BindingAdapter
import com.example.playgroundapp.R
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("setError")
fun TextInputLayout.error(errorMessageId: Int?) {

    if (errorMessageId != null) {
        error = resources.getString(errorMessageId)
        isErrorEnabled = true
        errorIconDrawable = null
    } else {
        if (error != resources.getString(R.string.register_activity_fragment_password_helper_text)) {
            isErrorEnabled = false
        }
        error = null
    }

}