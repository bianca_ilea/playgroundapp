package com.example.playgroundapp.extensions;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.playgroundapp.R;

public class ActivityUtils {

    public enum FragmentAnimationType {
        SlideInSlideOut
    }

    public static void addFragmentToActivity (@NonNull FragmentManager fragmentManager,
                                              @NonNull Fragment fragment,
                                              int frameId,
                                              Boolean addToBackStack,
                                              FragmentAnimationType... fragmentAnimationType) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(fragmentAnimationType != null && fragmentAnimationType.length > 0) {
            switch (fragmentAnimationType[0]) {
                case SlideInSlideOut:
                    transaction.setCustomAnimations(
                            R.anim.right_slide_in,
                            R.anim.right_slide_out,
                            R.anim.right_slide_in,
                            R.anim.right_slide_out
                    );
                    break;
            }
        }
        transaction.add(frameId, fragment);
        if (addToBackStack) {
            transaction.addToBackStack(String.valueOf(fragment.getId()));
        }
        transaction.commit();
    }
}
