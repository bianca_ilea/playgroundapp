package com.example.playgroundapp.activities.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.playgroundapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}