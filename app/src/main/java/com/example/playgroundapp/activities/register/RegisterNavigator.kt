package com.example.playgroundapp.activities.register

interface RegisterNavigator {

    fun goToSuccessPage()

    fun navigateToMainPage()
}