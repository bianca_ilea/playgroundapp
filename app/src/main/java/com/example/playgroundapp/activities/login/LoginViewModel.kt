package com.example.playgroundapp.activities.login

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.playgroundapp.base.BaseViewModel
import com.example.playgroundapp.repository.UserRepository
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent

class LoginViewModel : BaseViewModel() {

    val username = ObservableField("")

    val password = ObservableField("")

    private val repository: UserRepository by KoinJavaComponent.inject(UserRepository::class.java)

    private val _navigationCommands = MutableLiveData<LoginCommand>()
    val navigationCommands : LiveData<LoginCommand> = _navigationCommands

    val goToSignUp = {
        _navigationCommands.value = LoginCommand(LoginNavigationCommand.navigateToSignUp)
    }

    val login = {
        val introducedUsername = username.get()!!
        val introducedPassword = password.get()!!
        validateUsernameAndPassword(introducedUsername, introducedPassword)
    }

    private fun validateUsernameAndPassword(username: String, password: String){
        viewModelScope.launch {
            if(repository.areCredentialsValid(username, password)){
                _navigationCommands.value = LoginCommand(LoginNavigationCommand.navigateToMainActivity)
            }
        }
    }


}