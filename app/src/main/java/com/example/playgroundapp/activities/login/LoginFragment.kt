package com.example.playgroundapp.activities.login

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.playgroundapp.R
import com.example.playgroundapp.activities.login.LoginNavigationCommand.*
import com.example.playgroundapp.base.BaseFragment
import com.example.playgroundapp.databinding.FragmentLoginBinding
import org.koin.android.ext.android.inject

class LoginFragment : BaseFragment<LoginViewModel, FragmentLoginBinding>(R.layout.fragment_login) {

    private var listener: LoginNavigator? = null

    override val viewModel: LoginViewModel by inject()

    companion object {
        fun newInstance() = LoginFragment()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupObservers()
        binding.viewModel = viewModel
        // TODO: Use the ViewModel
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = if (context is LoginNavigator) {
            context
        } else {
            throw RuntimeException("$context must implement LoginNavigator")
        }
    }

    private fun setupObservers() {
        val navigationCommandObserver: Observer<LoginCommand> = Observer { navigationCommandAndObject: LoginCommand? ->
            if (navigationCommandAndObject != null) {
                when (navigationCommandAndObject.command) {
                    navigateToForgotPassword -> listener?.unGoToForgotPassword()
                    navigateToSignUp -> listener?.onGoToSignUp()
                    navigateToMainActivity -> listener?.navigateToMainPage()
                }
            }
        }
        viewModel.navigationCommands.observe(viewLifecycleOwner, navigationCommandObserver)
    }
}