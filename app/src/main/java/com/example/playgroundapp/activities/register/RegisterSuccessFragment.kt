package com.example.playgroundapp.activities.register

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.playgroundapp.R
import com.example.playgroundapp.base.BaseFragment
import com.example.playgroundapp.databinding.FragmentRegisterBinding
import com.example.playgroundapp.databinding.FragmentRegisterSuccessBinding
import org.koin.android.ext.android.inject


class RegisterSuccessFragment : BaseFragment<RegisterSuccessViewModel, FragmentRegisterSuccessBinding>(R.layout.fragment_register_success) {

    override val viewModel: RegisterSuccessViewModel by inject()

    private var listener: RegisterNavigator? = null

    companion object {

        fun newInstance(): RegisterSuccessFragment {
            val fragment = RegisterSuccessFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = if (context is RegisterNavigator) {
            context
        } else {
            throw RuntimeException("$context must implement RegisterNavigator")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel
        setupObservers()
    }

    private fun setupObservers() {

        viewModel.successCommand.observe(viewLifecycleOwner, {
            if (it) {
                listener?.navigateToMainPage()
            }
        })

    }
}