package com.example.playgroundapp.activities.register

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.playgroundapp.R
import com.example.playgroundapp.base.BaseViewModel
import com.example.playgroundapp.database.entity.user.UserEntity
import com.example.playgroundapp.repository.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent.inject

class RegisterViewModel : BaseViewModel() {

    private val repository: UserRepository by inject(UserRepository::class.java)

    val firstName = ObservableField("")

    val lastName = ObservableField("")

    val username = ObservableField("")
    val usernameError: ObservableField<Int> = ObservableField()

    val password = ObservableField("")

    val confirmPassword = ObservableField("")

    val email = ObservableField("")

    val dateOfBirth = ObservableField("")

    private val _navigationCommands = MutableLiveData<RegisterCommand>()
    val navigationCommands : LiveData<RegisterCommand> = _navigationCommands

    private val _registerSuccessCommand = MutableLiveData<Boolean>()
    val registerSuccessCommand: LiveData<Boolean> = _registerSuccessCommand

    fun pickDateOfBirth(){
        _navigationCommands.value = RegisterCommand(RegisterDialogCommand.showDatePickerDialog)
    }

    val submitButtonClicked = {
        register()
    }

    private fun register(){

        val introducedFirstname = firstName.get()!!
        val introducedLastname = lastName.get()!!
        val introducedUsername = username.get()!!
        val introducedPassword = password.get()!!
        val introducedEmail = email.get()!!

        viewModelScope.launch {
            if(!usernameAlreadyExists(introducedUsername)) {
                _registerSuccessCommand.value = true
                repository.insert(UserEntity(0, introducedFirstname, introducedLastname, introducedUsername, introducedPassword, introducedEmail))
            } else {
                usernameError.set(R.string.username_already_exists)
            }
        }

    }

    private suspend fun usernameAlreadyExists(username: String): Boolean {
        var userAlreadyExists = false

        val waitFor = CoroutineScope(Dispatchers.IO).async {
            userAlreadyExists = repository.userAlreadyExists(username)
        }
        waitFor.await()

        return userAlreadyExists
    }

}