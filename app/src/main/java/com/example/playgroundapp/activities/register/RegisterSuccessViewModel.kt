package com.example.playgroundapp.activities.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.playgroundapp.base.BaseViewModel

class RegisterSuccessViewModel : BaseViewModel() {

    private val _successCommand = MutableLiveData<Boolean>()
    val successCommand: LiveData<Boolean> = _successCommand

    val continueClick = {
        signin()
    }

    private fun signin(){
        _successCommand.postValue(true)
    }
}