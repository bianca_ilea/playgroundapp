package com.example.playgroundapp.activities.register

import android.app.DatePickerDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import com.example.playgroundapp.R
import com.example.playgroundapp.base.BaseFragment
import com.example.playgroundapp.databinding.FragmentRegisterBinding
import org.koin.android.ext.android.inject

@RequiresApi(Build.VERSION_CODES.N)
class RegisterFragment : BaseFragment<RegisterViewModel, FragmentRegisterBinding>(R.layout.fragment_register) {

    override val viewModel: RegisterViewModel by inject()

    private var listener: RegisterNavigator? = null

    companion object {
        fun newInstance() = RegisterFragment()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupObservers()
        binding.viewModel = viewModel
        // TODO: Use the ViewModel
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = if (context is RegisterNavigator) {
            context
        } else {
            throw RuntimeException("$context must implement RegisterNavigator")
        }
    }

    private fun setupObservers() {
        viewModel.navigationCommands.observe(viewLifecycleOwner, { navigationCommandAndObject: RegisterCommand? ->
            if (navigationCommandAndObject != null) {
                when (navigationCommandAndObject.command) {
                    RegisterDialogCommand.showDatePickerDialog -> this.showDatePickerDialog()
                }
            }
        })

        viewModel.registerSuccessCommand.observe(viewLifecycleOwner, {
            if(it) {
                listener?.goToSuccessPage()
            }
        })
    }

    private fun showDatePickerDialog(){
        val datePickerDialog = DatePickerDialog(requireContext())
        datePickerDialog.setOnDateSetListener { _, year, month, dayOfMonth -> viewModel.dateOfBirth.set("$dayOfMonth/${month+1}/$year") }
        datePickerDialog.show()
    }
}