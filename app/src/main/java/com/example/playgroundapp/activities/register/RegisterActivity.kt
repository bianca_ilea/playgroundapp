package com.example.playgroundapp.activities.register

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.example.playgroundapp.R
import com.example.playgroundapp.activities.main.MainActivity
import com.example.playgroundapp.base.BaseActivity
import com.example.playgroundapp.databinding.ActivityRegisterBinding
import com.example.playgroundapp.extensions.ActivityUtils
import org.koin.android.ext.android.inject

class RegisterActivity : BaseActivity<RegisterViewModel, ActivityRegisterBinding>(R.layout.activity_register), RegisterNavigator {

    override val viewModel: RegisterViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        findOrCreateViewFragment()
    }

    private fun findOrCreateViewFragment(): Fragment {
        var activityFragment = supportFragmentManager.findFragmentById(R.id.fragment_container_register)
        if (activityFragment == null) {
            activityFragment = RegisterFragment.newInstance()
            addFragment(R.id.fragment_container_register, activityFragment)
        }
        return activityFragment
    }

    override fun goToSuccessPage() {
        ActivityUtils.addFragmentToActivity(
                supportFragmentManager,
                RegisterSuccessFragment.newInstance(),
                R.id.fragment_container_register,
                false,
                ActivityUtils.FragmentAnimationType.SlideInSlideOut
        )
    }

    override fun navigateToMainPage() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}