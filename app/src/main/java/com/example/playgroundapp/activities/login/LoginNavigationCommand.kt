package com.example.playgroundapp.activities.login

enum class LoginNavigationCommand {
    navigateToSignUp,
    navigateToForgotPassword,
    navigateToMainActivity
}