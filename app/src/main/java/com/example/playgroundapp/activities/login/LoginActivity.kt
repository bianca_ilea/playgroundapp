package com.example.playgroundapp.activities.login

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.playgroundapp.R
import com.example.playgroundapp.activities.main.MainActivity
import com.example.playgroundapp.activities.register.RegisterActivity
import com.example.playgroundapp.base.BaseActivity
import com.example.playgroundapp.databinding.ActivityLoginBinding
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity<LoginViewModel, ActivityLoginBinding>(R.layout.activity_login), LoginNavigator {

    override val viewModel: LoginViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        findOrCreateViewFragment()
    }

    private fun findOrCreateViewFragment(): Fragment {
        var activityFragment = supportFragmentManager.findFragmentById(R.id.fragment_container_login)
        if (activityFragment == null) {
            activityFragment = LoginFragment.newInstance()
            addFragment(R.id.fragment_container_login, activityFragment)
        }
        return activityFragment
    }

    override fun onGoToSignUp() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun unGoToForgotPassword() {
        TODO("Not yet implemented")
    }

    override fun navigateToMainPage() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}