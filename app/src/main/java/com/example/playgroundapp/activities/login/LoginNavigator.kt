package com.example.playgroundapp.activities.login

interface LoginNavigator {

    fun onGoToSignUp()

    fun unGoToForgotPassword()

    fun navigateToMainPage()
}