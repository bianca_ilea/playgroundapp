package com.example.playgroundapp.di

import androidx.room.Room
import com.example.playgroundapp.activities.login.LoginViewModel
import com.example.playgroundapp.activities.register.RegisterSuccessViewModel
import com.example.playgroundapp.activities.register.RegisterViewModel
import com.example.playgroundapp.database.AppDatabase
import com.example.playgroundapp.repository.UserRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { LoginViewModel() }
    viewModel { RegisterViewModel() }
    viewModel { RegisterSuccessViewModel() }
}

val repoModule = module {
    single { Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "playground_db").build() }
    factory { UserRepository(get()) }
}